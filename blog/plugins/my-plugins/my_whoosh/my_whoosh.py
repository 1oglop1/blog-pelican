from pelican import signals
from pelican.generators import Generator
import bs4
from pathlib import Path
import whoosh.index as w_index
import whoosh.fields as w_fields
import shutil
from whoosh.analysis import StemmingAnalyzer
import os

# from .old_jan import add_reader

stem_ana = StemmingAnalyzer()
SCHEMA = w_fields.Schema(
    title=w_fields.TEXT(analyzer=stem_ana, stored=True),
    content=w_fields.TEXT(analyzer=stem_ana, stored=True),
    url=w_fields.TEXT(stored=True),
    category=w_fields.TEXT(stored=True),
    tags=w_fields.KEYWORD(stored=True),
)

INDEX_DIR = Path()


def prepare_index_dir(index_dir: Path):
    if not index_dir.exists():
        index_dir.mkdir(parents=True, exist_ok=True)

    for item in index_dir.glob("**/*"):
        if item.is_file():
            item.unlink()
        elif item.is_dir():
            shutil.rmtree(item)


def get_index(path: Path) -> w_index.Index:
    try:
        return w_index.open_dir(path, schema=SCHEMA, indexname='blog')
    except w_index.IndexError:
        return w_index.create_in(path, SCHEMA, indexname='blog')


class MyGenerator(Generator):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.index_path = INDEX_DIR
        self.index_path.mkdir(parents=True, exist_ok=True)
        self.index = get_index(self.index_path)
        # print(self.settings)

    def create_page_document(self, page):
        # title, content, url, category, tags
        content = page.content
        soup = bs4.BeautifulSoup(content, 'html.parser')
        text = soup.get_text()
        # print(page)
        # print(self.settings)

        with self.index.writer() as writer:
            writer.add_document(
                title=page.title,
                content=text,
                url=page.url,
                category=page.category.name,
                tags=','.join((tag.name for tag in page.tags))
            )

    def generate_output(self, writer):
        # print("opt run")
        pages = self.context['pages'] + self.context['articles']
        for page in pages:
            self.create_page_document(page)


def get_generators(obj):
    # define a new generator here if you need to
    return MyGenerator


def test(sender):
    global INDEX_DIR
    INDEX_DIR = Path(sender.settings.get('WHOOSH_INDEX_DIRECTORY')).absolute()
    # print("{} initialized !!".format(sender))
    # print(sender.settings)
    prepare_index_dir(INDEX_DIR)


def register():
    # signals.readers_init.connect(add_reader)

    signals.initialized.connect(test)

    signals.get_generators.connect(get_generators)
