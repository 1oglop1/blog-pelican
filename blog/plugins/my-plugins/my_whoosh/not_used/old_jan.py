from pelican import signals

from pelican import signals
from pelican.readers import BaseReader, RstReader, parse_path_metadata

# from pelican.writers import Writer
# from sphinx.builders.text import TextBuilder, TextTranslator, TextWriter
from docutils.core import Publisher
from docutils.utils import new_document
import docutils
from .text import TextWriter, TextTranslator


# Create a new reader class, inheriting from the pelican.reader.BaseReader
class NewReader(RstReader):
    enabled = True  # Yeah, you probably want that :-)
    writer_class = TextWriter
    field_body_translator_class = TextTranslator
    # The list of file extensions you want this reader to match with.
    # If multiple readers were to use the same extension, the latest will
    # win (so the one you're defining here, most probably).
    file_extensions = ['rst']

    # You need to have a read method, which takes a filename and returns
    # some content and the associated metadata.
    def read(self, source_path):
        """Parses restructured text"""
        pub = self._get_publisher(source_path)
        parts = pub.writer.parts
        content = parts.get('body')

        metadata = self._parse_metadata(pub.document, source_path)
        metadata.setdefault('title', parts.get('title'))

        return content, metadata

    # def read2(self, filename):
    #     document = new_document(filename)
    #     # print(filename)
    #     pub = docutils.core.Publisher(
    #         writer=self.writer_class(document),
    #         source_class=RstReader.FileInput,
    #         destination_class=docutils.io.StringOutput,
    #         # settings=self.settings
    #     )
    #     pub.set_components('standalone', 'restructuredtext', 'txt')
    #     pub.process_programmatic_settings(None, self.settings, None)
    #     pub.writer.translate()
    #     pub.set_source(source_path=filename)
    #     pub.publish()
    #     print('x')
    #     return pub
        # pub.process_programmatic_settings(None, env.settings, None)
        # pub2.set_source(source_path=filename)
        #
        # pub2.publish()

        # return pub2, {}


def add_reader(readers):
    readers.reader_classes['rst'] = NewReader


def test(sender):
    print("{} initialized !!".format(sender))


def register():
    signals.initialized.connect(test)
    # signals.readers_init.connect(add_reader)
