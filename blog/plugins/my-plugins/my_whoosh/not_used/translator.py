class SphinxTranslator(nodes.NodeVisitor):
    """A base class for Sphinx translators.

    This class provides helper methods for Sphinx translators.

    .. note:: The subclasses of this class might not work with docutils.
              This class is strongly coupled with Sphinx.
    """

    def __init__(self, document: nodes.document, builder: "Builder") -> None:
        super().__init__(document)
        self.builder = builder
        self.config = builder.config
        self.settings = document.settings