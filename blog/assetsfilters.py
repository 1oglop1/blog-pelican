from webassets.filter import Filter, ExternalTool
from webassets.utils import working_directory
import os


class NoopFilter(Filter):
    name = 'noop'

    def output(self, _in, out, **kwargs):
        print("OUTPUT NOOP")
        out.write(_in.read())

    def input(self, _in, out, **kwargs):
        print("INPUT NOOP")
        out.write(_in.read())


class Terser(ExternalTool):
    name = 'terser'
    options = {
        'terser': ('binary', 'TERSER_BIN'),
        'extra_args': 'TERSER_ARGS',
    }

    def resolve_source(self, path):
        return self.ctx.resolver.resolve_source(self.ctx, path)

    def _apply(self, in_, out, source_path=None, **kw):
        # Set working directory to the source file so that includes are found
        args = self.parse_binary(self.terser or 'terser')

        args.append('-c')
        args.append('-m')
        # print(self.extra_args)
        if self.extra_args:
            args.extend(self.extra_args)
        if source_path:
            with working_directory(filename=source_path):
                self.subprocess(args, out, in_)
        else:
            self.subprocess(args, out, in_)

    def output(self, _in, out, **kwargs):
        """Output filter is applied after all files in the bundle are merged."""

        self._apply(_in, out)


class FooFilter(Filter):
    def __init__(self, *args, **kwargs):
        self.args, self.kwargs = args, kwargs

    def unique(self):
        return self.args, self.kwargs
