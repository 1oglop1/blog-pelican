import os

from starlette.applications import Starlette
from starlette.responses import PlainTextResponse

app = Starlette(debug=True)

@app.route('/{path:path}')
async def index(request, path=""):
    return PlainTextResponse('\n'.join(os.listdir('.')))




if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=8000)
