import os
from pathlib import Path
from typing import Dict, Generator, List

import whoosh.fields as w_fields
import whoosh.index as w_index
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from whoosh.analysis import StemmingAnalyzer
from whoosh.qparser import MultifieldParser, OrGroup
from whoosh.highlight import HtmlFormatter

app = Starlette(debug=True)

INDEX_DIR = Path(os.environ.get('WHOOSH_INDEX_DIRECTORY'))

stem_ana = StemmingAnalyzer()
SCHEMA = w_fields.Schema(
    title=w_fields.TEXT(analyzer=stem_ana, stored=True),
    content=w_fields.TEXT(analyzer=stem_ana, stored=True),
    url=w_fields.TEXT(stored=True),
    category=w_fields.TEXT(stored=True),
    tags=w_fields.KEYWORD(stored=True),
)

INDEX = w_index.open_dir(INDEX_DIR, schema=SCHEMA, indexname='blog')
HF = HtmlFormatter(tagname="span", classname="tipue_search_content_bold")


def prepare_results(results) -> Generator[Dict, None, None]:
    for hit in results:
        fields = hit.fields()
        highlight = hit.highlights('content')

        if highlight:
            fields['content'] = highlight
        else:
            fields['content'] = fields['content'][:181]

        yield fields


def findit(query_string: str) -> List[Dict]:
    """Find in index and return results as generator of dicts."""
    with INDEX.searcher() as searcher:
        parser = MultifieldParser(["title", "content"], schema=SCHEMA, group=OrGroup.factory(0.9))
        query = parser.parse(query_string)
        search_results = searcher.search(query)
        search_results.formatter = HF
        return list(prepare_results(search_results))


# @app.route('/search')
# @app.route('/<path:path>')
# async def index(request, path=""):
#     print('args', request.args)
#     query = request.args.get('q')
#     if query:
#         print("q:", query)
#         results = findit(query)
#         print(results)
#         return json(results)
#
#     return json({})

@app.route('/{path:path}')
async def index(request, path=''):
    # print(request.__dict__)
    query = request.query_params.get('q')
    if query:
        print("q:", query)
        results = findit(query)
        print('found results', results)
        return JSONResponse({'results': results})

    print('pp', request.path_params)
    print(type(request.query_params), request.query_params)

    return JSONResponse({'params': f"{request.query_params.get('q')}"})


if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app, host='0.0.0.0', port=8000)
