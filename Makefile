BLOG_DIR = blog
API_DIR = api
NOW_OPTS=

ifdef NOW_TOKEN
NOW_OPTS += --token $(NOW_TOKEN)
endif


serve:
	$(MAKE) -C $(BLOG_DIR) serve

html:
	$(MAKE) -C $(BLOG_DIR) html

deploy-api:
	@now $(NOW_OPTS) $(API_DIR)

deploy-blog:
	$(MAKE) -C $(BLOG_DIR) publish
	@now $(NOW_OPTS) $(BLOG_DIR)/public

deploy: deploy-blog

hello:
	@echo $(NOW_OPTS)

clean:
	$(MAKE) -C $(BLOG_DIR) clean

.PHONY: clean html deploy deploy-api deploy-blog