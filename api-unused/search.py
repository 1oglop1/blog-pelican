from pathlib import Path
from typing import Dict, Generator, List
import os
import whoosh.fields as w_fields
import whoosh.index as w_index
from sanic import Sanic
from sanic.response import text, json
from whoosh.analysis import StemmingAnalyzer
from whoosh.qparser import MultifieldParser, OrGroup

app = Sanic()

# HERE = Path(__file__).parent
#
INDEX_DIR = Path('whoosh_index')

stem_ana = StemmingAnalyzer()
SCHEMA = w_fields.Schema(
    title=w_fields.TEXT(analyzer=stem_ana, stored=True),
    content=w_fields.TEXT(analyzer=stem_ana, stored=True),
    url=w_fields.TEXT(stored=True),
    category=w_fields.TEXT(stored=True),
    tags=w_fields.KEYWORD(stored=True),
)

INDEX = w_index.open_dir(INDEX_DIR, schema=SCHEMA, indexname='blog')


def prepare_results(results) -> Generator[Dict, None, None]:
    for hit in results:
        fields = hit.fields()
        highlight = hit.highlights('content')

        if highlight:
            fields['content'] = highlight
        else:
            fields['content'] = fields['content'][:181]

        yield fields


def findit(query_string: str) -> List[Dict]:
    """Find in index and return results as generator of dicts."""
    with INDEX.searcher() as searcher:
        parser = MultifieldParser(["title", "content"], schema=SCHEMA, group=OrGroup.factory(0.9))
        query = parser.parse(query_string)
        search_results = searcher.search(query)
        return list(prepare_results(search_results))


@app.exception(Exception)
def server_error_handler(request, exception):
    if request is not None:
        msg = request.url + ' ' + traceback.format_exc()
    elif isinstance(exception, RequestTimeout):
        return response.text('timeout', status=504)
    else:
        msg = str(exception) + ' ' + traceback.format_exc()
    asyncio.ensure_future(fluent.write_debug('error', msg))

@app.route('/')
@app.route('/<path:path>')
async def index(request, path=""):
    query = request.args.get('q')
    if query:
        print("q:", query)
        results = findit(query)
        print(results)
        return json(results)

    return json({})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8001, debug=True)
